# -*- coding: UTF-8 -*-
# Environment: python2.7 32bit
# pip install pyautogui
# pip install pyinstaller
#
# install pywin32
# 64bit: pywin32-221.win-amd64-py2.7.exe
# 32bit: pywin32-219.win32-py2.7.exe
# at https://sourceforge.net/projects/pywin32/
#
# install pyHook (64bit has bug: runtime error R6031)
# 64bit:pyHook-1.5.1-cp27-cp27m-win_amd64.whl
# 32bit:pyHook-1.5.1-cp27-cp27m-win32.whl)
# cmd: pip install pyHook-1.5.1-cp27-cp27m-win32.whl
# at https://www.lfd.uci.edu/~gohlke/pythonlibs/
# 
# Date: 2020/07/02
# Author: DJoke
import os
import threading, json
import pyautogui
import pythoncom, pyHook, win32api
from time import sleep
RecordFile = "click_record.json"
LISTENING = False               # Global: 是否正在監聽鍵盤事件
Recording = False               # Global: 是否正在監聽滑鼠按鍵位置
Starting = False                # Global: 是否正在進行滑鼠連點
Record_list = list()            # Global: 紀錄滑鼠按下的座標
hm = None   # HookManager

### Parameter ###
os.system('mode con: cols=52 lines=13')     # 設定command line視窗大小
pyautogui.PAUSE = 1             # 設定每個PyAutoGUI函式呼叫在執行動作後暫停的秒數
pyautogui.FAILSAFE = False      # 當滑鼠一道螢幕左上角(0,0)時，觸發pyautogui的failsafeexception異常

### Show Log ###
def show_wellcome():
    print u'---------------------------------------------'
    print u'------------- Auto Clicker 1.1 --------------'
    print u'---------------------------------------------'
    return
    
def show_hint():
    print u'--- 操作指南 --------------------------------'
    print u'---------------------------------------------'
    print u' F10: 開始或暫停連點'
    print u' F12: 設定紀錄點'
    print u' Esc: 結束程式'
    print u'---------------------------------------------'
    print
    return

### Record ###
def load_record():
    global Record_list
    try:
        f = open(RecordFile, "r")
        data = f.read()
        f.close()
        Record_list = json.loads(data)
    except:
        Record_list = list()
    return
    
def save_record():
    global Record_list
    f = open(RecordFile, "w+")
    f.write(json.dumps(Record_list))
    f.close()
    return

### Event ###
def onMouseEvent(event):
    global Record_list
    # 監聽滑鼠事件   
    # print "MessageName:",event.MessageName
    # print "Message:", event.Message
    # print "Time:", event.Time
    # print "Window:", event.Window
    # print "WindowName:", event.WindowName
    # print "Position:", event.Position
    # print "Wheel:", event.Wheel
    # print "Injected:", event.Injected
    # print"---"
    # 返回 True 以便將事件傳給其它處理程式
    # 注意，這兒如果返回 False ，則滑鼠事件將被全部攔截
    # 也就是說你的滑鼠看起來會僵在那兒，似乎失去響應了
    name = event.MessageName
    if(name == "mouse left down"):
        position = event.Position
        Record_list.append(position)
        print u"\rpos:", position,u"已記錄:",len(Record_list),"     \r",
    return True

def onKeyboardEvent(event):
    global Recording, Starting, Record_list, hm
    # 監聽鍵盤事件   
    # print "MessageName:", event.MessageName   
    # print "Message:", event.Message   
    # print "Time:", event.Time   
    # print "Window:", event.Window   
    # print "WindowName:", event.WindowName   
    # print "Ascii:", event.Ascii, chr(event.Ascii)   
    # print "Key:", event.Key   
    # print "KeyID:", event.KeyID   
    # print "ScanCode:", event.ScanCode   
    # print "Extended:", event.Extended   
    # print "Injected:", event.Injected   
    # print "Alt", event.Alt   
    # print "Transition", event.Transition   
    # print "---"   
    # 同滑鼠事件監聽函式的返回值
    key = event.Key
    if(key == "Escape"):
        # print "[thd] Escape"
        # import ctypes
        # ctypes.windll.user32.PostQuitMessage(0)   # 與下方同等效益
        win32api.PostQuitMessage()  # 離開pythoncom.PumpMessages()
    elif(key == "F10"):
        if(Recording):
            return True
        # print "[thd] F10"
        if(Starting):
            print u"\r[暫停]                       \r",
            Starting = False
        else:
            print u"\r[開始] 點擊F10暫停           \r",
            load_record()
            Starting = True
    elif(key == "F12"):
        # print "[thd] F12"
        if(Starting):
            return True
        if(Recording):
            print u"\r[紀錄] Stop recording!       \r",
            Recording = False
            hm.UnhookMouse()
            # print "Mouse Record:",Record_list
            save_record()
        else:
            print u"\r[紀錄] Start to record!      \r",
            Recording = True
            Record_list = list()
            # 監聽所有滑鼠事件
            hm.MouseAll = onMouseEvent
            # 設定滑鼠"鉤子"
            hm.HookMouse()
    return True

def listen_key():
    global Recording, Starting, LISTENING, hm
    # 建立一個"鉤子"管理物件
    hm = pyHook.HookManager()
    # 監聽所有鍵盤事件
    hm.KeyDown = onKeyboardEvent
    # 設定鍵盤"鉤子"
    hm.HookKeyboard()
    # 監聽所有滑鼠事件
    # hm.MouseAll = onMouseEvent
    # 設定滑鼠"鉤子"
    # hm.HookMouse()
    
    # 進入迴圈，如不手動關閉，程式將一直處於監聽狀態
    try:
        LISTENING = True
        print u"\r F10開始連點 F12紀錄點擊     \r",
        # print "[thd] Start to listen Keyboard!"
        pythoncom.PumpMessages()    # 無限迴圈，直到呼叫win32api.PostQuitMessage()
    except Exception as e:
        print "\n\n[thd] Exception:",e
    # 暫停監聽
    if(Recording):
        Recording = False
        hm.UnhookMouse()
    Starting = False
    LISTENING = False
    hm.UnhookKeyboard()
    print "\n\n[thd] Stop listen."
    return

if __name__ == "__main__":
    show_wellcome()
    show_hint()
    key_thd = threading.Thread(target = listen_key)
    key_thd.start()
    sleep(1)
    try:
        while True:
            if(not LISTENING):
                break
            if(Starting):
                i = 0
                count = len(Record_list)
                if(count==0):
                    Starting = False
                    print u"\r[結束] 無任何紀錄            \r",
                while True:
                    if(not Starting):
                        break
                    if(i==count):
                        i=0
                    width = Record_list[i][0]
                    height = Record_list[i][1]
                    pyautogui.click(width, height)
                    i += 1
    except Exception as e:
        print u"\n[Main] Exception:",e
    sleep(0.1)
    print u"\n[Main] Join thread."
    key_thd.join()
    print u"[Main] 結束程式."
    # raw_input("Press Enter to Exit.")